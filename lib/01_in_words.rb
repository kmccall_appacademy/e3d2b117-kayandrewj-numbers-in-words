class Fixnum

WORDS = {
-9 => "",
-8 => "thousand ",
-7 => "million ",
-6=> "billion ",
-5 => "trillion ",
1 => "one",
2 => "two",
3 => "three",
4 => "four",
5 => "five",
6 => "six",
7 => "seven",
8 => "eight",
9 => "nine",
10 => "ten",
11 => "eleven",
12 => "twelve",
13 => "thirteen",
14 => "fourteen",
15 => "fifteen",
16 => "sixteen",
17 => "seventeen",
18 => "eighteen",
19 => "nineteen",
20 => "twenty",
30 => "thirty",
40 => "forty",
50 => "fifty",
60 => "sixty",
70 => "seventy",
80 => "eighty",
90 => "ninety"
}

def make_array
  self.to_s.gsub(/(\d)(?=(.{3})+(?!\d))/, "\\1 ").split(" ").map {|s| s.to_i}
end

def builder(set_of_three, magnitude = "") # builds small string, up to value 999
  string = ""
  current = set_of_three
  while current > 0
    if current > 99
      string += "#{WORDS[(current/100)]} hundred " #gets the hundreds place and finds the word in the hash
      current = current - ((current/100)*100) #subtracts that many hundreds from current
    elsif current > 19
      string += "#{WORDS[(current - current%10)]} "
      current = current - (current - current%10)
    elsif current > 9 #handles teens
      string += "#{WORDS[current]} "
      current -= current #breaks loop and heads to return stuff at bottom of method
    else
      string += "#{WORDS[current]} "
      current = current - current
    end
  end
  string += magnitude
  string
end

def in_words
  return "zero" if self == 0
  string = []
  working_arr = self.make_array
  working_arr.reverse.each_with_index do |set, i|
    next if set == 000
    string << builder(set, WORDS[i-9])
  end
  string.reverse.join.strip
end
end
